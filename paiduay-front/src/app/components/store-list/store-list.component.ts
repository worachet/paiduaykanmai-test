import { Component, OnInit, ViewChild } from '@angular/core';
import { StoreForm } from 'src/app/models/forms/store.form';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface StoreElement {
  _id: string;
  name: string;
  tel: number;
  address: string;
  description: string;
}

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class StoreListComponent implements OnInit {
  public storeForm = new StoreForm().getStoreForm();

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public columnsToDisplay = ['name', 'tel', 'product'];
  public expandedElement: StoreElement | null;

  @ViewChild('matPaginator', { static: true })
  private paginator!: MatPaginator;
  @ViewChild('matSort', { static: true })
  private sort!: MatSort;
  
  constructor(
    private httpService: HttpService,
  ) { }

  ngOnInit(): void {
    this.onLoad();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onReset() {
    this.storeForm.reset();
  }

  async onLoad() {
    this.dataSource = new MatTableDataSource<any>(await this.httpService.post(`${environment.api_store}/read`, {}));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  async onSubmit() {
    await this.httpService.post(`${environment.api_store}/create`, this.storeForm.value);
    this.onReset();
    this.onLoad();
  }

  onEdit(t: StoreElement) {
    this.storeForm.setValue(t);
  }

}