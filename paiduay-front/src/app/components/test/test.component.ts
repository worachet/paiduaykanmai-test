import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  public t1Res = null;
  public t1Form = new FormGroup({
    t1: new FormControl(null),
    t2: new FormControl(null)
  });

  public t2Res = null;
  public t2Form = new FormGroup({
    t1: new FormControl(null)
  });

  public t3Res = null;
  public t3Form = new FormGroup({
    t1: new FormControl(null),
    t2: new FormControl(null),
    t3: new FormControl(null),
    t4: new FormControl(null)
  });

  constructor(
    private httpService: HttpService,
  ) { }

  ngOnInit(): void {
  }

  async onTest1() {
    this.t1Res = await this.httpService.post(`${environment.api_test}/first`, this.t1Form.value);
  }
  async onTest2() {
    this.t2Res = await this.httpService.post(`${environment.api_test}/second`, this.t2Form.value);
  }
  async onTest3() {
    this.t3Res = await this.httpService.post(`${environment.api_test}/thrid`, this.t3Form.value);
  }
}
