import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GroupForm } from 'src/app/models/forms/group.form';

export interface GroupElement {
  _id: string;
  name: string;
  description: string;
}

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class GroupListComponent implements OnInit {
  public groupForm = new GroupForm().getGroupForm();

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public columnsToDisplay = ['name', 'description', 'edit'];
  public expandedElement: GroupElement | null;

  @ViewChild('matPaginator', { static: true })
  private paginator!: MatPaginator;
  @ViewChild('matSort', { static: true })
  private sort!: MatSort;

  constructor(
    private httpService: HttpService,
  ) { }

  ngOnInit(): void {
    this.onLoad();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onReset() {
    this.groupForm.reset();
  }

  async onLoad() {
    this.dataSource = new MatTableDataSource<any>(await this.httpService.post(`${environment.api_group}/read`, {}));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  async onSubmit() {
    await this.httpService.post(`${environment.api_group}/create`, this.groupForm.value);
    this.onReset();
    this.onLoad();
  }

  onEdit(t: GroupElement) {
    this.groupForm.setValue(t);
  }
}
