import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ProductForm } from 'src/app/models/forms/product.form';
import { ActivatedRoute, Router } from '@angular/router';

export interface ProductElement {
  _id: string;
  store_name: string;
  group_name: string;
  name: string;
  price: string;
  unit: string;
  description: string;
}

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ProductListComponent implements OnInit {
  private storeName = null;
  public groups = null;
  public productForm = new ProductForm().getProductForm();

  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  public columnsToDisplay = ['name', 'store_name', 'group_name', 'delete'];
  public expandedElement: ProductElement | null;

  @ViewChild('matPaginator', { static: true })
  private paginator!: MatPaginator;
  @ViewChild('matSort', { static: true })
  private sort!: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private httpService: HttpService,
  ) { }

  ngOnInit(): void {
    this.storeName = this.route.snapshot.paramMap.get('name');
    this.onLoad();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onStorePath() {
    this.router.navigate([`/store`]);
  }
  onReset() {
    this.productForm.reset();
  }

  async onLoad() {
    let obj = {};
    if(this.storeName) {
      obj = {store_name: this.storeName}
    }
    this.groups = await this.httpService.post(`${environment.api_group}/read`, {});
    this.dataSource = new MatTableDataSource<any>(await this.httpService.post(`${environment.api_product}/read`, obj));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  async onSubmit() {
    if(this.storeName) {
      this.productForm.patchValue({store_name: this.storeName});
      await this.httpService.post(`${environment.api_product}/create`, this.productForm.value);
      this.onReset();
      this.onLoad();
    }else {
      alert('เลือกร้านค้า ที่หน้า"ข้อมูลร้านค้า" ก่อนเพิ่มข้อมูลสินค้า');
    }
  }

  onEdit(t: ProductElement) {
    this.productForm.setValue(t);
  }

  async onDelete(id: string) {
    if(confirm("ต้องลบสินค้าหรือไม่")) {
      await this.httpService.put(`${environment.api_product}/delete`, {id: id});
      this.onReset();
      this.onLoad();
    }
  }

}
