import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupListComponent } from './components/group-list/group-list.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { StoreListComponent } from './components/store-list/store-list.component';
import { TestComponent } from './components/test/test.component';


const routes: Routes = [
  {
    path: '',
    component: StoreListComponent
  },
  {
    path: 'store', 
    component: StoreListComponent
  },
  {
    path: 'product', 
    component: ProductListComponent
  },
  {
    path: 'group', 
    component: GroupListComponent
  },
  {
    path: 'test', 
    component: TestComponent
  },
  {
    path: 'product/:name', 
    component: ProductListComponent
  },
// -----------------------------------------------
  {
    path: '**', redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
