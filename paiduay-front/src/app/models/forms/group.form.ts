import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class GroupForm {
    private groupForm = new FormGroup({
        _id: this.c,
        name: this.c,
        description: this.c
    });

    getGroupForm() {
        return this.groupForm;
    }

    get c() {
        return new FormControl(null);
    }
}