import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class StoreForm {
    private storeForm = new FormGroup({
        _id: this.c,
        name: this.c,
        description: this.c,
        tel: this.c,
        address: this.c
    });

    getStoreForm() {
        return this.storeForm;
    }

    get c() {
        return new FormControl(null);
    }
}