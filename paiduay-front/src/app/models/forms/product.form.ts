import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class ProductForm {
    private productForm = new FormGroup({
        _id: this.c,
        store_name: this.c,
        group_name: this.c,
        name: this.c,
        price: this.c,
        unit: this.c,
        description: this.c
    });

    getProductForm() {
        return this.productForm;
    }

    get c() {
        return new FormControl(null);
    }
}