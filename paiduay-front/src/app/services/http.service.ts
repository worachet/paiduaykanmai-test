import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private httpHeader = {headers: new HttpHeaders().set('Content-Type', 'application/json')};

  constructor(private http: HttpClient) { }

  get(url: string) {
    return this.http.get<any>(url, this.httpHeader).toPromise();
  }
  post(url: string, obj: {}) {
    return this.http.post<any>(url, obj, this.httpHeader).toPromise();
  }
  put(url: string, obj: {}) {
    return this.http.put<any>(url, obj, this.httpHeader).toPromise();
  }

}
