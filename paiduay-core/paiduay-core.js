import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();
app.use(bodyParser.json());
app.use(cors());

import store from './src/modules/store/api.js';
app.use('/store', store);

const port = process.env.PORT || 5000;
const server = app.listen(port, () => {
  console.log('Listening on port ' + server.address().port);
});