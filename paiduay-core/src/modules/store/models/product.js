import mongoose from 'mongoose';
const { Schema } = mongoose;

let obj = new Schema({
    store_name: String,
    group_name: String,
    name: String,
    description: String,
    price: String,
    unit: String
},{
    collection: 'product',
    versionKey: false
});

export default mongoose.model('product', obj);