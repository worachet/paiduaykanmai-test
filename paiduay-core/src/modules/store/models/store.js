import mongoose from 'mongoose';
const { Schema } = mongoose;

let obj = new Schema({
   name: String,
   description: String,
   tel: String,
   address: String
},{
   collection: 'store',
   versionKey: false
});

export default mongoose.model('store', obj);