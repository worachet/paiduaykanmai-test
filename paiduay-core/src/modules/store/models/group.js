import mongoose from 'mongoose';
const { Schema } = mongoose;

let obj = new Schema({
    name: String,
    description: String
},{
    collection: 'group',
    versionKey: false
});

export default mongoose.model(obj.collection, obj);