let functions = {};

functions.first = (num, str) => {
    let sum = 0;
    if(num > 0 && str) {
        str = str.toLowerCase();
        for(let i=0; i<num; i++) {
            if(i+1 < num) {
                if(str.charAt(i) === str.charAt(i+1)) {
                    sum+=1;
                }
            }
        }
    }
    return sum;
}

functions.second = (qty) => {
    let sum = 0;
    while(qty > 0) {
        if(qty >= 100) {
            qty = qty - 100;
            sum+=1;
        }else if(qty >= 20) {
            qty = qty - 20;
            sum+=1;
        }else if(qty >= 10) {
            qty = qty - 10;
            sum+=1;
        }else if(qty >= 5) {
            qty = qty - 5;
            sum+=1;
        }else if(qty >= 1) {
            qty = qty - 1;
            sum+=1;
        }
    }
    return sum;
}

functions.thrid = (n, a, b, c) => {
    let count = 0;
    let count2 = 1;
    let count3 = 1;
    let sum = 0;
    if((n, a, b, c) > 0 && (a, b, c) <= 4000) {
        let arr = [a, b, c].sort(function(a, b){return a-b});
        while(true) {
            while(true) {
                while(true) {
                    if(sum == n) break;
                    if(sum > n) {
                        count = 0;
                        sum = 0;
                        break;
                    }
                    count+=1;
                    sum = sum + arr[0];
                }
                if(sum == n) break;
                if(sum > n) {
                    count = 0;
                    sum = 0;
                    break;
                }
                for(let i=0; i<count2; i++) {
                    count+=1;
                    sum = sum + arr[1];
                }
                count2+=1;
            }
            if(sum == n) break;
            if(sum > n) {
                count = 0;
                count2 = 0;
                count3 = 0;
                sum = 0;
                break;
            }
            for(let j=0; j<count3; j++) {
                count+=1;
                sum = sum + arr[2];
            }
            count3+=1;
        }
        while(true) {
            while(true) {
                if(sum == n) break;
                if(sum > n) {
                    count = 0;
                    sum = 0;
                    break;
                }
                count+=1;
                sum = sum + arr[0];
            }
            if(sum == n) break;
            if(sum > n) {
                count = 0;
                count2 = 0;
                count3 = 0;
                sum = 0;
                break;
            }
            for(let j=0; j<count3; j++) {
                count+=1;
                sum = sum + arr[2];
            }
            count3+=1;
        }
        while(true) {
            while(true) {
                if(sum == n) break;
                if(sum > n) {
                    count = 0;
                    sum = 0;
                    break;
                }
                for(let i=0; i<count2; i++) {
                    count+=1;
                    sum = sum + arr[1];
                }
                count2+=1;
            }
            if(sum == n) break;
            if(sum > n) {
                count = 0;
                count2 = 0;
                count3 = 0;
                sum = 0;
                break;
            }
            for(let j=0; j<count3; j++) {
                count+=1;
                sum = sum + arr[2];
            }
            count3+=1;
        }
    }
    return count;
}

export default functions;