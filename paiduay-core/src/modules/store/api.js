import express from 'express';
import mongoose from 'mongoose';
import config from './config/database.js';

import storeRoute from './routes/store.route.js';
import productRoute from './routes/product.route.js';
import groupRoute from './routes/group.route.js';
import testRoute from './routes/test.route.js';

mongoose.Promise = global.Promise;
mongoose.connect(`${config.mongoDB}/paiduay`, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false }).then(
  () => { console.log(`Paiduay database is connected`); },
  err => { console.log(`Can not connect to the database ${err}`); }
);

const app = express();
app.use('/store', storeRoute);
app.use('/product', productRoute);
app.use('/group', groupRoute);
app.use('/test', testRoute);

export default app;