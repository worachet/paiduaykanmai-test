import { Router } from 'express';
const routes = Router();

import ProductModel from '../models/product.js';

routes.route('/count').get((req, res) => {
  ProductModel.countDocuments({}, count, (err) => { 
    if(err)
      console.error(`Failed to count documents: ${err}`);
    else
      res.status(200).json(count);
  });
});

routes.route('/read').post((req, res) => {
  ProductModel.find(req.body, (err, docs) => {
    if(err)
      console.error(`Failed to read documents: ${err}`);
    else
      res.status(200).json(docs);
  }).sort({name: 1});
});

routes.route('/create').post((req, res) => {
  ProductModel.findOne({_id: req.body._id}, (err, docs) => {
    if(err) {
      console.error(`Failed to read documents: ${err}`);
    }else if(docs) {
      let product = new ProductModel(req.body);
      ProductModel.updateOne({_id: req.body._id}, product).then((data) => {
        res.status(200).json(data.length);
      }).catch(err => {
        console.error(`Failed to insert documents: ${err}`);
      });
    }else {
      let product = new ProductModel(req.body);
      ProductModel.insertMany(product).then((data) => {
        res.status(200).json(data.length);
      }).catch(err => {
        console.error(`Failed to insert documents: ${err}`);
      });
    }
  });
});

routes.route('/delete').put((req, res) => {
  ProductModel.deleteOne({_id: req.body.id}, (err) => { 
    if(err)
      console.error(`Failed to delete documents: ${err}`);
    else
      res.status(200).json("Deleted");
  });
});

export default routes;