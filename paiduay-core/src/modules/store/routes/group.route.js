import { Router } from 'express';
const routes = Router();

import GroupModel from '../models/group.js';

routes.route('/count').get((req, res) => {
  GroupModel.countDocuments({}, (err) => { 
    if(err)
      console.error(`Failed to count documents: ${err}`);
    else
      res.status(200).json(docs);
  });
});

routes.route('/read').post((req, res) => {
  GroupModel.find(req.body, (err, docs) => {
    if(err)
      console.error(`Failed to read documents: ${err}`);
    else
      res.status(200).json(docs);
  }).sort({name: 1});
});

routes.route('/create').post((req, res) => {
  GroupModel.findOne({_id: req.body._id}, (err, docs) => {
    if(err) {
      console.error(`Failed to read documents: ${err}`);
    }else if(docs) {
      let group = new GroupModel(req.body);
      GroupModel.updateOne({_id: req.body._id}, group).then((data) => {
        res.status(200).json(data.length);
      }).catch(err => {
        console.error(`Failed to insert documents: ${err}`);
      });
    }else {
      let group = new GroupModel(req.body);
      GroupModel.insertMany(group).then((data) => {
        res.status(200).json(data.length);
      }).catch(err => {
        console.error(`Failed to insert documents: ${err}`);
      });
    }
  });
});

export default routes;