import { Router } from 'express';
const routes = Router();

import TestService from '../services/test.js';

routes.route('/first').post(async (req, res) => {
  res.json(TestService.first(req.body.t1, req.body.t2));
});

routes.route('/second').post(async (req, res) => {
  res.json(TestService.second(req.body.t1));
});

routes.route('/thrid').post(async (req, res) => {
  // res.json(TestService.thrid(5, 5, 3, 2));
  // res.json(TestService.thrid(7, 5, 5, 2));
  // res.json(TestService.thrid(4, 4, 4, 4));
  // res.json(TestService.thrid(3455, 244, 3301, 3));
  res.json(TestService.thrid(Number(req.body.t1), Number(req.body.t2), Number(req.body.t3), Number(req.body.t4)));
});

export default routes;