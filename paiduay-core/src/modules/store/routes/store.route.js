import { Router } from 'express';
const routes = Router();

import StoreModel from '../models/store.js';

routes.route('/count').get((req, res) => {
  StoreModel.countDocuments({}, (err) => { 
    if(err)
      console.error(`Failed to count documents: ${err}`);
    else
      res.status(200).json(docs);
  });
});

routes.route('/read').post((req, res) => {
  StoreModel.find(req.body, (err, docs) => {
    if(err)
      console.error(`Failed to read documents: ${err}`);
    else
      res.status(200).json(docs);
  }).sort({name: 1});
});

routes.route('/create').post((req, res) => {
  StoreModel.findOne({_id: req.body._id}, (err, docs) => {
    if(err) {
      console.error(`Failed to read documents: ${err}`);
    }else if(docs) {
      let store = new StoreModel(req.body);
      StoreModel.updateOne({_id: req.body._id}, store).then((data) => {
        res.status(200).json(data.length);
      }).catch(err => {
        console.error(`Failed to insert documents: ${err}`);
      });
    }else {
      let store = new StoreModel(req.body);
      StoreModel.insertMany(store).then((data) => {
        res.status(200).json(data.length);
      }).catch(err => {
        console.error(`Failed to insert documents: ${err}`);
      });
    }
  });
});

export default routes;